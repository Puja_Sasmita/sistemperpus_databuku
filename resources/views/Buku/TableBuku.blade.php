@extends('Layout.main')

@section('head')
    <h1 class="m-0">Data Buku</h1>
@endsection

@section('sidebar')
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-open">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/mahasiswa" class="nav-link">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Data Mahasiswa
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/buku" class="nav-link active">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Data Buku
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/Peminjaman" class="nav-link">
            <i class="nav-icon far fa-window-restore"></i>
            <p>
              Data Peminjaman
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                  <a href="/FormBuku" class="btn btn-success mt-2 mx-3 col-2"><i class="fas fa-plus-circle"></i>   Tambah Data</a>
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Judul Buku</th>
                      <th>Pengarang</th>
                      <th>Penerbit</th>
                      <th>Tahun Terbit</th>
                      <th>Tebal</th>
                      <th>Isbn</th>
                      <th>Stok Buku</th>
                      <th>Biaya Sewa Harian</th>
                    </tr>
                    </thead>
                    <?php $i=1;?>
                    <tbody>
                        @foreach ($data as $datas)
                            <tr>
                                <td><?php echo($i); $i++; ?></td>
                                <td>{{$datas->judul_buku}}</td>
                                <td>{{$datas->pengarang}}</td>
                                <td>{{$datas->penerbit}}</td>
                                <td>{{$datas->tahun_terbit}}</td>
                                <td>{{$datas->tebal}}</td>
                                <td>{{$datas->isbn}}</td>
                                <td>{{$datas->stok_buku}}</td>
                                <td>{{$datas->biaya_sewa_harian}}</td>
                                <td>
                                    <a href="/Edit{{$datas->id_buku}}" class="btn btn-warning"><i class="fas fa-edit"></i></a> 
                                    <a href="/delete{{$datas->id_buku}}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
@endsection