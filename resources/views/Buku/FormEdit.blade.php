@extends('Layout.main')

@section('head')
    <h1 class="m-0">Ubah Data Buku</h1>
@endsection

@section('sidebar')
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-open">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/mahasiswa" class="nav-link">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Data Mahasiswa
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/buku" class="nav-link active">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Data Buku
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/Peminjaman" class="nav-link">
            <i class="nav-icon far fa-window-restore"></i>
            <p>
              Data Peminjaman
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card card-primary">
                    <form action="/Edit/{{$data->id_buku}}" method="post">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Judul Buku</label>
                          <input type="text" class="form-control" name="judul_buku" placeholder="Putu Indah" value="{{$data->judul_buku}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pengarang</label>
                            <input type="text" class="form-control" name="pengarang" placeholder="191505" value="{{$data->pengarang}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Penerbit</label>
                            <input type="text" class="form-control" name="penerbit" placeholder="Putu@gmail.com" value="{{$data->penerbit}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Tahun Terbit</label>
                            <input type="text" class="form-control" name="tahun_terbit" placeholder="082237***" value="{{$data->tahun_terbit}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Tebal</label> 
                            <input type="text" class="form-control" name="tebal" placeholder="Sistem Nuklir" value="{{$data->tebal}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Isbn</label>
                            <input type="text" class="form-control" name="isbn" placeholder="Teknik Nuklir" value="{{$data->isbn}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Stok Buku</label>
                            <input type="text" class="form-control" name="stok_buku" placeholder="Teknik Perminyakan" value="{{$data->stok_buku}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Biaya Sewa Harian</label>
                            <input type="text" class="form-control" name="biaya_sewa_harian" placeholder="Teknik Perminyakan" value="{{$data->biaya_sewa_harian}}" required>
                          </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-warning">Ubah</button>
                        <a href="/buku" class="btn btn-default">Batal</a>
                      </div>
                    </form>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection