@extends('Layout.main')

@section('head')
    <h1 class="m-0">Ubah Data Mahasiswa</h1>
@endsection

@section('sidebar')
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-open">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/mahasiswa" class="nav-link active">
            <i class="nav-icon fas fa-user-graduate"></i>
            <p>
              Data Mahasiswa
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/buku" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
              Data Buku
            </p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="/Peminjaman" class="nav-link">
            <i class="nav-icon far fa-window-restore"></i>
            <p>
              Data Peminjaman
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card card-primary">
                    <form action="/Edit/{{$data->id_mahasiswa}}" method="post">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Mahasiswa</label>
                          <input type="text" class="form-control" name="nama" placeholder="Putu Indah" value="{{$data->nama}}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">NIM Mahasiswa</label>
                            <input type="text" class="form-control" name="nim" placeholder="191505" value="{{$data->nim}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Putu@gmail.com" value="{{$data->email}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Nomor Telepon</label>
                            <input type="text" class="form-control" name="nomor" placeholder="082237***" value="{{$data->no_telp}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Program Studi</label> 
                            <input type="text" class="form-control" name="prodi" placeholder="Sistem Nuklir" value="{{$data->prodi}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Jurusan</label>
                            <input type="text" class="form-control" name="jurusan" placeholder="Teknik Nuklir" value="{{$data->jurusan}}" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Fakultas</label>
                            <input type="text" class="form-control" name="fakultas" placeholder="Teknik Perminyakan" value="{{$data->fakultas}}" required>
                          </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-warning">Ubah</button>
                        <a href="/mahasiswa" class="btn btn-default">Batal</a>
                      </div>
                    </form>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection