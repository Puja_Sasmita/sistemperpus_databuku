<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaCont;
use App\Http\Controllers\BukuCont;
use App\Http\Controllers\IndexCont;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexCont::class, 'show']);

Route::get('/mahasiswa', [MahasiswaCont::class, 'show']);
Route::get('/FormMahasiswa', [MahasiswaCont::class, 'form']);
Route::post('/FormMahasiswa', [MahasiswaCont::class, 'add']);
Route::get('/Edit{id}', [MahasiswaCont::class, 'formedit']);
Route::post('/Edit/{id}', [MahasiswaCont::class, 'edit']);
Route::get('/delete{id}', [MahasiswaCont::class, 'delete']);

Route::get('/buku', [BukuCont::class, 'show']);
Route::get('/FormBuku', [BukuCont::class, 'form']);
Route::post('/FormBuku', [BukuCont::class, 'add']);
Route::get('/Edit{id}', [BukuCont::class, 'formedit']);
Route::post('/Edit/{id}', [BukuCont::class, 'edit']);
Route::get('/delete{id}', [BukuCont::class, 'delete']);

