<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuCont extends Controller
{
    public function show()
    {
        $data = DB::table('buku')->get();
        return view('buku.TableBuku',['data' => $data]);
    }

    public function form()
    {
        return view('buku.FormInputData');
    }

    public function add(Request $add)
    {
        //Masukkan data yang terekam di formulir ke variabel penampung
        $judul_buku = $add->judul_buku;
        $pengarang = $add->pengarang;
        $penerbit = $add->penerbit;
        $tahun_terbit = $add->tahun_terbit;
        $tebal = $add->tebal;
        $isbn = $add->isbn;
        $stok_buku = $add->stok_buku;
        $biaya_sewa_harian = $add->biaya_sewa_harian;

        //Nambah data nama, nim, kelas, prodi, fakultas
        DB::table('buku')->insert(
            [
                'judul_buku' => $judul_buku,
                'pengarang' => $pengarang,
                'penerbit' => $penerbit,
                'tahun_terbit' => $tahun_terbit,
                'tebal' => $tebal,
                'isbn' => $isbn,
                'stok_buku' => $stok_buku,
                'biaya_sewa_harian' => $biaya_sewa_harian
            ]
        );

        //redirect ke halaman utama
        return redirect('/buku');
    }

    public function formedit(Int $id)
    {
        $data = DB::table('buku')->where('id_buku', $id)->first();
        return view('buku.FormEdit', ['data'=> $data]);
    }

    public function edit(Request $data,Int $id)
    {
        $data = DB::table('buku')->where('id_buku', $id)->update(
            [
                'judul_buku' => $data->judul_buku,
                'pengarang' => $data->pengarang,
                'penerbit' => $data->penerbit,
                'tahun_terbit' => $data->tahun_terbit,
                'tebal' => $data->tebal,
                'isbn' => $data->isbn,
                'stok_buku' => $data->stok_buku,
                'biaya_sewa_harian' => $data->biaya_sewa_harian
            ]
            );

            return redirect('/buku');
    }

    public function delete(Int $id)
    {
        $delete = DB::table('buku')->where('id_buku', $id)->delete();
        return redirect('/buku');
    }
}
