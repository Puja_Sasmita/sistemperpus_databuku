<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaCont extends Controller
{
    public function show()
    {
        $data = DB::table('mahasiswa')->get();
        return view('Mahasiswa.TableMahasiswa',['data' => $data]);
    }

    public function form()
    {
        return view('Mahasiswa.FormInputData');
    }

    public function add(Request $add)
    {
        //Masukkan data yang terekam di formulir ke variabel penampung
        $nama = $add->nama;
        $nim = $add->nim;
        $email = $add->email;
        $no_telp = $add->nomor;
        $prodi = $add->prodi;
        $jurusan = $add->jurusan;
        $fakultas = $add->fakultas;

        //Nambah data nama, nim, kelas, prodi, fakultas
        DB::table('mahasiswa')->insert(
            [
                'nama' => $nama,
                'nim' => $nim,
                'email' => $email,
                'no_telp' => $no_telp,
                'jurusan' => $jurusan,
                'prodi' => $prodi,
                'fakultas' => $fakultas
            ]
        );

        //redirect ke halaman utama
        return redirect('/mahasiswa');
    }

    public function formedit(Int $id)
    {
        $data = DB::table('mahasiswa')->where('id_mahasiswa', $id)->first();
        return view('Mahasiswa.FormEdit', ['data'=> $data]);
    }

    public function edit(Request $data,Int $id)
    {
        $data = DB::table('mahasiswa')->where('id_mahasiswa', $id)->update(
            [
                'nama' => $data->nama,
                'nim' => $data->nim,
                'email' => $data->email,
                'no_telp' => $data->nomor,
                'jurusan' => $data->jurusan,
                'prodi' => $data->prodi,
                'fakultas' => $data->fakultas
            ]
            );

            return redirect('/mahasiswa');
    }

    public function delete(Int $id)
    {
        $delete = DB::table('mahasiswa')->where('id_mahasiswa', $id)->delete();
        return redirect('/mahasiswa');
    }
}
